<?php
return [
    'Name' => 'Namew',
    'api' => [
        'login_success' => 'iniciar sesión correctamente',
        'email_phone_incorrect_format' => 'Correo electrónico/número de teléfono formato incorrecto',
        'account_not_exist' => 'La dirección de correo electrónico o el número de teléfono ingresados no coinciden con los registros',
        'account_deactivated' => '¡Lo siento! Su cuenta ha sido inactiva por el sistema',
        'otp_sent_success' => 'Otp enviado con éxito',
        'register_success' => 'Registrado correctamente.',
        'user_not_found' => 'Usuario no encontrado',
        'logged_out' => 'Cerrar sesión con éxito',
        'notification_status_update' => 'Actualización de estado de notificación'
    ],
    'plan' => [
        'choose_plan' => 'Elige el plan',
        'skip' => 'saltar',
        'monthly_price' => 'Precio mensual',
        'buy_now' => 'Compra ahora'
    ],
    'login' => [
        'welcome' => 'Bienvenido a AWE',
        'select_lang' => 'Seleccione el idioma de la aplicación',
        'enter_email_phone' => 'Enter your email/mobile number',
        'email_required' => 'The email field is required.',
        'email_invalid' => 'Email/Phone number incorrect format',
        'password_required' => 'The password field is required.',
        'min_8_password' => 'The password must be at least 8 characters.',
        'password' => 'Password',
        'forgot_password' => 'Forgot Password?',
        'sign_in' => 'Sign In',
        'or_continue' => 'Or Countinue with',
        'dont_have_account' => "Don't have an account?",
        'sign_up' => "Sign Up here",
    ],
    'register' => [
        'secondary_title' => 'Create an account to access the full \n version with all features.',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'email' => 'Email Address',
        'send_otp' => 'Send OTP',
        'mobile' => 'Mobile Number',
        'enter_email_otp' => 'Enter Email OTP',
        'enter_mobile_otp' => 'Enter Mobile OTP',
        'password' => 'Password',
        'c_password' => 'Confirm Password',
        'referral_code' => 'Referral Code',
        'already_account' => 'Already have an account?',
        'sign_in' => 'Sign In',
        'by_clicking' => 'By Clicking',
        'agree' => ', you are agree to our Terms & Conditions',
        'first_name_required' => 'The first name field is required.',
        'first_name_min_2' => 'The first name must be at least 2 characters.',
        'last_name_required' => 'The last name field is required.',
        'email_required' => 'The email address field is required.',
        'mobile_required' => 'The mobile number field is required.',
        'email_otp_required' => 'The email OTP is required.',
        'mobile_otp_required' => 'The mobile OTP field is required.',
        'password_required' => 'The password field is required.',
        'c_password_required' => 'The confirm password field is required.',
        'min_8_password' => 'The password must be at least 8 characters.',
        'password_cpassword_same' => 'Password & confirm password should be the same',
        'valid_otp' => 'Please enter valid OTP',
    ],
    'forgot_password' => [
        'secondary_title' => 'Ingrese su dirección de correo electrónico o número de teléfono móvil para recuperar su cuenta.',
        'send' => 'enviar'
    ],
    'recommendation' => 'Recomendación',
    'view_more' => 'Ver más',
    'favorite_video_success' => 'Me gustó el video',
    'favorite_video_remove' => 'Vídeo no me gusta',
    'favorite_video_exits' => 'el video ya existe',
    'video_not_found' => 'Video no encontrado',
    'no_data_found'=>'Datos no disponibles',
    'mylist_video_success' => 'Mi lista de videos se agregó con éxito',
    'mylist_video_remove' => 'El video de mi lista se eliminó con éxito',
    'feedback_success' => 'Gracias por tus comentarios',
    'rating_exits' => 'La calificación de video ya existe',
    'rating_success' => 'Calificación de video Agregar con éxito',
    'faq_does_not_exist' => 'Preguntas frecuentes no existe',
    'user_age_restriction_success' => 'Los datos de restricción de edad del usuario se agregaron correctamente',
    'playback_setting_update_success' => 'Actualización de la configuración de reproducción con éxito',
    'playback_setting_not_update' => 'Configuración de reproducción no actualizada',
    'category_data_added' => 'Datos de categoría de usuario agregados con éxito',
    'category_not_found' => 'Categoría de título seleccionada no encontrada',
    'deregister_device_sucess' => 'Dispositivo cancelado con éxito',
    'device_not_found' => 'Dispositivo no encontrado',
    'subscription_plan_added_sucess' => 'Plan de suscripción agregado con éxito',
    'activity_log_added_success' => 'Registro de actividad añadido con éxito',
    'enter_valid_user_or_content' => 'Introduzca un usuario o contenido válido',
    'invalid_phone' => 'Numero de telefono invalido',
    'phone_not_registered' => 'El número de teléfono no está registrado',
    'change_password_link_sent' => 'Cambiar enlace de contraseña enviado en el correo electrónico ingresado',
    'email_address_not_registered' => 'La dirección de correo electrónico no está registrada',
    'title_restiction_not_found' => 'No se encontró la restricción del título seleccionado',
    'movie_tvshow_not_found' => 'Película/programa de televisión seleccionado no encontrado',
    'lock_added' => 'Bloqueo agregado',
    'verified' => 'Verificada',
    'incorrect_pin' => 'PIN incorrecto. Inténtalo de nuevo.',
    'disable_pin' => 'Su PIN se desactivó con éxito.',
    'change_pin' => 'Su pin se cambió con éxito.',
    'something_went_wrong' => 'Algo salió mal',
    'home'=>[
        'categories' =>  'Categorías',
        'whatch_now' =>  'Ver ahora',
        'home' =>  'Casa',
        'kids' =>  'Niños',
        'view_more' => 'Ver más'
    ],
    'content_detail'=>[
        'trailer' =>  'Remolque',
        'my_list' =>  'Mi lista',
        'liked' =>  'Me gusta',
        'dislike' =>  'Disgusto',
        'play' =>  'Play',
        'share' =>  'Share',
        'episodes' =>  'Episodios',
        'more_like_this' =>  'más como esto',
        'cast' => 'Elenco',
        'starring' => 'Protagonista',
        'this_series_is' => 'esta serie es',
        'genres' => 'Géneros',
        'languages' => 'Idiomas',
        'audio' => 'Audio',
        'subtitles' => 'subtítulos',
        'available_languages' => 'Idiomas Disponibles',
        'available_languages_sub_text' => 'Los idiomas se pueden cambiar mientras se reproduce el video',
    ],
    'faq' => [
        'help' => 'ayuda'
    ],
];
