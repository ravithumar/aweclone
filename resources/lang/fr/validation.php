<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'La :attribute doit être accepté.',
    'active_url' => 'La :attribute n'."'".'est pas une URL valide.',
    'after' => 'La :attribute doit être une date après :date.',
    'after_or_equal' => 'La :attribute doit être une date postérieure ou égale à :date.',
    'alpha' => 'La :attribute ne doit contenir que des lettres.',
    'alpha_dash' => 'La :attribute ne doit contenir que des lettres, des chiffres, des tirets et des traits de soulignement.',
    'alpha_num' => 'La :attribute ne doit contenir que des lettres et des chiffres.',
    'array' => 'La :attribute doit être un tableau.',
    'before' => 'La :attribute doit être une date avant :date.',
    'before_or_equal' => 'La :attribute doit être une date antérieure ou égale à :date.',
    'between' => [
        'numeric' => 'La :attribute Doit être entre :min et :max.',
        'file' => 'La :attribute Doit être entre :min et :max kilo-octets.',
        'string' => 'La :attribute Doit être entre :min et :max personnages.',
        'array' => 'La :attribute doit avoir entre :min et :max éléments.',
    ],
    'boolean' => 'La :attribute champ doit être vrai ou faux.',
    // 'confirmed' => 'The :attribute confirmation does not match.',
    'confirmed' => 'Le mot de passe et le mot de passe de confirmation doivent être identiques',
    'date' => 'La :attribute Ce n'."'".'est pas une date valide.',
    'date_equals' => 'La :attribute doit être une date égale à :date.',
    'date_format' => 'La :attribute ne correspond pas au format :format.',
    'different' => 'La :attribute et :other doit être différent.',
    'digits' => 'La :attribute doit être :digits chiffres.',
    'digits_between' => 'La :attribute Doit être entre :min et :max chiffres.',
    'dimensions' => 'La :attribute a des dimensions d'."'".'image non valides.',
    'distinct' => 'La :attribute champ a une valeur en double.',
    'email' => 'La :attribute Doit être une adresse e-mail valide.',
    'ends_with' => 'La :attribute doit se terminer par l'."'".'un des following: :values.',
    'exists' => 'La choisie :attribute est invalide.',
    'file' => 'La :attribute doit être un fichier.',
    'filled' => 'La :attribute le champ doit avoir une valeur.',
    'gt' => [
        'numeric' => 'La :attribute doit être supérieur à :value.',
        'file' => 'La :attribute doit être supérieur à :value kilo-octets.',
        'string' => 'La :attribute doit être supérieur à :value personnages.',
        'array' => 'La :attribute doit avoir plus de :value éléments.',
    ],
    'gte' => [
        'numeric' => 'La :attribute doit être supérieur à ou égal :value.',
        'file' => 'La :attribute doit être supérieur à ou égal :value kilo-octets.',
        'string' => 'La :attribute doit être supérieur à ou égal :value personnages.',
        'array' => 'La :attribute doit avoir :value articles ou plus.',
    ],
    'image' => 'La :attribute doit être une image.',
    'in' => 'La choisie :attribute est invalide.',
    'in_array' => 'La :attribute le champ n'."'".'existe pas dans :other.',
    'integer' => 'La :attribute Doit être un entier.',
    'ip' => 'La :attribute doit être une adresse IP valide.',
    'ipv4' => 'La :attribute doit être une adresse IPv4 valide.',
    'ipv6' => 'La :attribute doit être une adresse IPv6 valide.',
    'json' => 'La :attribute doit être une chaîne JSON valide.',
    'lt' => [
        'numeric' => 'La :attribute doit être inférieur à :value.',
        'file' => 'La :attribute doit être inférieur à :value kilo-octets.',
        'string' => 'La :attribute doit être inférieur à :value personnages.',
        'array' => 'La :attribute doit avoir moins de :value éléments.',
    ],
    'lte' => [
        'numeric' => 'La :attribute doit être inférieur à or equal :value.',
        'file' => 'La :attribute doit être inférieur à or equal :value kilo-octets.',
        'string' => 'La :attribute doit être inférieur à or equal :value personnages.',
        'array' => 'La :attribute ne doit pas avoir plus de :value éléments.',
    ],
    'max' => [
        'numeric' => 'La :attribute ne doit pas être supérieur à :max.',
        'file' => 'La :attribute ne doit pas être supérieur à :max kilo-octets.',
        'string' => 'La :attribute ne doit pas être supérieur à :max personnages.',
        'array' => 'La :attribute ne doit pas avoir plus de :max éléments.',
    ],
    'mimes' => 'La :attribute doit être un fichier de type: :values.',
    'mimetypes' => 'La :attribute doit être un fichier de type: :values.',
    'min' => [
        'numeric' => 'La :attribute doit être au moins :min.',
        'file' => 'La :attribute doit être au moins :min kilo-octets.',
        'string' => 'La :attribute doit être au moins :min personnages.',
        'array' => 'La :attribute doit avoir au moins :min éléments.',
    ],
    'multiple_of' => 'La :attribute doit être un multiple de :value.',
    'not_in' => 'La choisie :attribute est invalide.',
    'not_regex' => 'La :attribute le format n'."'".'est pas valide.',
    'numeric' => 'La :attribute doit être un nombre.',
    'password' => 'La Le mot de passe est incorrect.',
    'present' => 'La :attribute champ doit être présent.',
    'regex' => 'La :attribute le format n'."'".'est pas valide.',
    'required' => 'La :attribute Champ requis.',
    'required_if' => 'La :attribute champ est obligatoire lorsque :other est :value.',
    'required_unless' => 'La :attribute champ est obligatoire sauf si :other est dans :values.',
    'required_with' => 'La :attribute champ est obligatoire lorsque :values est présent.',
    'required_with_all' => 'La :attribute champ est obligatoire lorsque :values sont présents.',
    'required_without' => 'La :attribute champ est obligatoire lorsque :values n'."'".'est pas présent.',
    'required_without_all' => 'La :attribute champ est obligatoire lorsque aucun de :values sont présents.',
    'prohibited' => 'La :attribute terrain est interdit.',
    'prohibited_if' => 'La :attribute champ est interdit lorsque :other est :value.',
    'prohibited_unless' => 'La :attribute champ est interdit sauf si :other est dans :values.',
    'same' => 'La :attribute et :other doit correspondre.',
    'size' => [
        'numeric' => 'La :attribute doit être :size.',
        'file' => 'La :attribute doit être :size kilo-octets.',
        'string' => 'La :attribute doit être :size personnages.',
        'array' => 'La :attribute doit contenir :size éléments.',
    ],
    'starts_with' => 'La :attribute doit commencer par l'."'".'un des following: :values.',
    'string' => 'La :attribute doit être une chaîne.',
    'timezone' => 'La :attribute doit être une zone valide.',
    'unique' => 'La :attribute a déjà été pris.',
    'uploaded' => 'La :attribute échec du téléchargement.',
    'url' => 'La :attribute le format n'."'".'est pas valide.',
    'uuid' => 'La :attribute doit être un UUID valide.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
