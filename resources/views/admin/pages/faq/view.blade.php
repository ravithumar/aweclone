@extends('admin.layouts.master')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('adminviewfaq')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>

    @php
    $question = $faq->getTranslations('question');
    $answer = $faq->getTranslations('answer');
    @endphp

	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>

                                <tr>
                                    <th class="text-nowrap" scope="row">Question English</th>
                                    <td colspan="5">{{ $question['en'] }}</td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Answer English</th>
                                    <td colspan="5">{{ $answer['en'] }}</td>
                                </tr>

                                <tr>
                                    <th class="text-nowrap" scope="row">Question Mandarin</th>
                                    <td colspan="5">{{ $question['md'] }}</td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Answer Mandarin</th>
                                    <td colspan="5">{{ $answer['md'] }}</td>
                                </tr>

                                <tr>
                                    <th class="text-nowrap" scope="row">Question Hindi</th>
                                    <td colspan="5">{{ $question['hi'] }}</td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Answer Hindi</th>
                                    <td colspan="5">{{ $answer['hi'] }}</td>
                                </tr>

                                <tr>
                                    <th class="text-nowrap" scope="row">Question Spanish</th>
                                    <td colspan="5">{{ $question['sp'] }}</td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Answer Spanish</th>
                                    <td colspan="5">{{ $answer['sp'] }}</td>
                                </tr>

                                <tr>
                                    <th class="text-nowrap" scope="row">Question French</th>
                                    <td colspan="5">{{ $question['fr'] }}</td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Answer French</th>
                                    <td colspan="5">{{ $answer['fr'] }}</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
			</div>	
		</div>
	</div>
</div>
@endsection