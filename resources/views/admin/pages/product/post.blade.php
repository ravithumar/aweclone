@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('admin-add-product')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>
    <?php
    $default_img_url = asset('images/logo.png');
    ?>
    <style type="text/css">
        .item-img{
            object-fit: cover;
            width: 150px;
        }
    </style>
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                <form action="{{ route('admin.product.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('POST')

                    <div class="row">
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="name">Name<span class="text-danger">*</span></label>
                                <input type="text" name="name" parsley-trigger="change" value="{{old('name')}}" required placeholder="Enter name" class="form-control" id="name">
                                @error('name')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="category_id">Select Category<span class="text-danger">*</span></label>
                                <select class="form-control select2" data-parsley-errors-container="#category_error" required name="category_id" id="category_id" data-placeholder="Select Category">
                                    <option selected disabled></option>
                                    @if(isset($category) && count($category) > 0)
                                        @foreach($category as $data)
                                            <option value="{{$data['id']}}">{{$data['name']}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div id="category_error"></div>
                                @error('category_id')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="discount">Discount</label>
                        <input type="text" name="discount" parsley-trigger="change" value="{{old('discount')}}"  placeholder="Enter Discount" class="form-control perc discount" id="name" min="0" max="100">
                        @error('discount')
                            <div class="error">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="business_discount">Business Discount</label>
                                <input type="text" name="business_discount" parsley-trigger="change" value="{{old('business_discount')}}" placeholder="Enter Business Discount" class="form-control perc discount" id="name" min="0" max="100">
                                @error('business_discount')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-12 mt-2 mb-1">
                            <h4 class="mt-0 mb-0 header-title">Inventory</h4>
                            <hr class="mt-1 mb-3">
                        </div>
                    </div>

                    <div class="row varient-group-div">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Quantity Info<span class="text-danger">*</span></label>
                                        <div>
                                            <input required type="text" name="quantity_info[]" class="form-control quantity_info" placeholder="Ex: 500">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Measurement<span class="text-danger">*</span></label>
                                        <div>
                                            <select class="form-control select2" data-parsley-errors-container="#measurement_error" required name="measurement_id[]" id="measurement_id" data-placeholder="Select Measurement">
                                                <option selected disabled></option>
                                                @if(isset($measurement) && count($measurement) > 0)
                                                    @foreach($measurement as $data)
                                                        <option value="{{$data['id']}}">{{$data['name']}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <div id="measurement_error"></div>
                                            @error('measurement_id')
                                                <div class="error">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Price<span class="text-danger">*</span></label>
                                        <div>
                                            <input required type="text" name="price[]" class="form-control price-touchspin" placeholder="Ex: 20" data-parsley-errors-container="#price-error">
                                        </div>
                                        <div id="price-error"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Business Price<span class="text-danger">*</span></label>
                                        <div>
                                            <input required type="text" name="business_price[]" class="form-control price-touchspin" placeholder="Ex: 20" data-parsley-errors-container="#b-price-error">
                                        </div>
                                        <div id="b-price-error"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- // work by rignesh -->
                            <div class="row">                                 
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Age</label>
                                        <div>
                                            <input type="number" name="age[]" class="form-control age" placeholder="Ex: 10">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Proof</label>
                                        <div>
                                            <input type="number" name="proof[]" class="form-control perc" placeholder="Ex: 20" min="0" max="100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- // work by rignesh -->
                            <div class="row">                                
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Item Code<span class="text-danger">*</span></label>
                                        <div>
                                            <input required type="text" name="item_code[]" class="form-control" placeholder="Ex: 20">
                                        </div>
                                    </div>
                                </div>  
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Get Image</label>
                                        <div>
                                            <div class="input-group mb-3">
                                              <input type="text" name="image_link[]" class="form-control img-link-txt" placeholder="Enter URL" aria-label="Enter URL" aria-describedby="basic-addon2">
                                              <div class="input-group-append">
                                                <button class="btn btn-primary get-img" type="button">Get</button>
                                              </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="uploaded_img[]" class="uploaded_img" value="">                                
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="text-center">
                                <img class="rounded mx-auto d-block border item-img default-img" src="<?php echo $default_img_url; ?>" height="150">
                                <input type="file" name="picture[]" accept="image/*" style="visibility:hidden; position: absolute;" class="input-file upload-img" required>
                            </div>
                        </div>
                        <div class="col-md-1">
                        </div>
                    </div>

                    <div id="variants">
                                <!-- Dynmaic  -->
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <button type="button" name="add_variant" class="btn btn-primary waves-effect waves-light" id="add_variant" title="Add More Attribute">
                                    Add More Attribute
                                </button>
                            </div>
                        </div>
                    </div> 


                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            Submit
                        </button>
                        <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                            Cancel
                        </button>
                    </div>

                </form>

				</div>
			</div>	
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
var default_img_url = '<?php echo $default_img_url; ?>';
var measurement = '<?php echo (isset($measurement) && count($measurement) > 0)?json_encode($measurement):""; ?>';
measurement = $.parseJSON(measurement);
console.log(measurement);
        
$(document).on('click', '.default-img', function(){

    $(this).next('input').click();
    //$(this).attr('src', default_img_url);
    return false;
});

var set_img;
$(document).on('change', '.upload-img', function() {
    readURL(this);
    set_img = '';
});

$(document).on('click', '.get-img', function(){

    var image_link = $(this).closest('.input-group').find('.img-link-txt').val();
    console.log(image_link);
    $this = $(this);

    if(image_link != ''){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });

        $.ajax({
            url: '/admin/get_image',
            type: 'post',
            data: {
                "image_link": image_link
            },
            success: function (returnData) {
                $('#loader').hide();
                returnData = $.parseJSON(returnData);
                if (typeof returnData != "undefined"){
                    
                    if(returnData.status == true){

                        $this.closest('.varient-group-div').find('.item-img').attr('src', returnData.image_full_link);
                        $this.closest('.varient-group-div').find('.input-file').removeAttr('required');
                        $this.closest('.varient-group-div').find('.uploaded_img').val(returnData.image_name);
                        //$('.item-img').attr('src', img);
                        //$('.input-file').removeAttr('required');
                        Notiflix.Notify.Success(returnData.message);
                    }else{
                        Notiflix.Notify.Failure(returnData.message);
                    }
                }
              
            }
          }); 
    }else{
        Notiflix.Notify.Failure('Enter URL of image.');
    }
    

});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            console.log($(input).closest('.text-center').find('.default-img'));

            $(input).closest('.text-center').find('.default-img').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

if(measurement != ''){
    var str = '';
    $.each(measurement, function(k, v) {
        str += '<option value='+v.id+'>'+v.name+'</option>';
    });
}

$(document).on('click','.remove-btn',function(){
    $(this).closest(".varient-row").remove();
});

var count = 1;
$(document).on('click','#add_variant',function(){

    count = count + 1;

    var variant_row = 
    '<div class="varient-row">'+
    '<hr>'+
        '<div class="row varient-group-div">'+
            '<div class="col-md-9">'+
                '<div class="row">'+
                    '<div class="col-lg-6">'+
                        '<div class="form-group">'+
                            '<label class="required">Quantity Info<span class="text-danger">*</span></label>'+
                            '<div>'+
                                '<input required type="text" name="quantity_info[]" class="form-control quantity_info" placeholder="Ex: 500">'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-lg-6">'+
                        '<div class="form-group">'+
                            '<label class="required">Measurement<span class="text-danger">*</span></label>'+
                            '<div>'+
                                '<select class="form-control select2" data-parsley-errors-container="#measurement_error'+count+'" required name="measurement_id[]" id="measurement_id'+count+'" data-placeholder="Select Measurement">'+
                                    '<option selected disabled></option>'+
                                    str+
                                '</select>'+
                                '<div id="measurement_error'+count+'"></div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="row">'+
                    '<div class="col-lg-6">'+
                        '<div class="form-group">'+
                            '<label class="required">Price<span class="text-danger">*</span></label>'+
                            '<div>'+
                            '<input required type="number" name="price[]" class="form-control price-touchspin" placeholder="Ex: 20" data-parsley-errors-container="#price-error'+count+'">'+
                            '</div>'+
                            '<div id="price-error'+count+'"></div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-lg-6">'+
                        '<div class="form-group">'+
                            '<label class="required">Business Price<span class="text-danger">*</span></label>'+
                            '<div>'+
                                '<input required type="number" name="business_price[]" class="form-control price-touchspin" placeholder="Ex: 20" data-parsley-errors-container="#b-price-error'+count+'">'+
                            '</div>'+
                            '<div id="b-price-error'+count+'"></div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                // work by rignesh
                '<div class="row">'+
                    '<div class="col-lg-6">'+
                        '<div class="form-group">'+
                            '<label class="required">Age</label>'+
                            '<div>'+
                                '<input type="number" name="age[]" class="form-control age" placeholder="Ex: 10">'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-lg-6">'+
                        '<div class="form-group">'+
                            '<label class="required">Proof</label>'+
                            '<div>'+
                                '<input type="number" name="proof[]" class="form-control perc" placeholder="Ex: 20"  min="0" max="100">'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="row">'+
                    '<div class="col-lg-6">'+
                        '<div class="form-group">'+
                            '<label class="required">Item Code<span class="text-danger">*</span></label>'+
                            '<div>'+
                                '<input required type="text" name="item_code[]" class="form-control" placeholder="Ex: 10">'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-lg-6">'+
                        '<div class="form-group">'+
                            '<label class="required">Get Image</label>'+
                            '<div>'+
                                '<div class="input-group mb-3">'+
                                  '<input type="text" name="image_link[]" class="form-control img-link-txt" placeholder="Enter URL" aria-label="Enter URL" aria-describedby="basic-addon2">'+
                                  '<div class="input-group-append">'+
                                    '<button class="btn btn-primary get-img" type="button">Get</button>'+
                                  '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<input type="hidden" name="uploaded_img[]" class="uploaded_img" value="">'+
                '</div>'+
                // work by rignesh
            '</div>'+
            '<div class="col-md-2">'+
                '<div class="text-center">'+
                    '<img class="rounded mx-auto d-block border item-img default-img" src="'+default_img_url+'" height="150">'+
                    '<input type="file" name="picture[]" accept="image/*" style="visibility:hidden; position: absolute;" class="input-file upload-img" required>'+
                '</div>'+
            '</div>'+
            '<div class="col-md-1">'+
                '<div class="d-flex h-100">'+
                    '<div class="row justify-content-center align-self-center">'+
                        '<label class="btn btn-danger waves-effect waves-light remove-btn remove" title="Remove"><i class="fas fa-times"></i></label>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
    '</div>';                           

    $('#variants').append(variant_row);
    $(".select2").select2();
    $(".age").TouchSpin({
        min: 0,
        max: 1000000000
    });
    $(".perc").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });
    $(".price-touchspin").TouchSpin({
        min: 0,
        max: 1000000000,
        step: 0.1,
        stepinterval: 50,
        decimals: 2,
        maxboostedstep: 10000000,
        prefix: '$'
    });


});


</script>
@endsection
