@extends('admin.layouts.master')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('admineditcontent')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                <form action="{{ route('admin.content.update',$content->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                @php

                

                $name = $content->getTranslations('name');
                $synopsis = $content->getTranslations('synopsis');

                $artist_ids = array();
                foreach ($content->content_cast as $key => $value){
                    $artist_ids[] = $value->artist_id;
                }

                $content_genres_ids = array();
                foreach ($content->content_genres as $key => $value){
                    $content_genres_ids[] = $value->genre_id;
                }

                $content_tags_ids = array();
                foreach ($content->content_tags as $key => $value){
                    $content_tags_ids[] = $value->tag_id;
                }

                @endphp

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name_en">Name English<span class="text-danger">*</span></label>
                                <input type="text" name="name_en" parsley-trigger="change" value="{{ $name['en'] }}" required placeholder="Enter Content English Name" class="form-control" id="name_en">
                                @error('name_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name_md">Name Mandarin<span class="text-danger">*</span></label>
                                <input type="text" name="name_md" parsley-trigger="change" value="{{$name['md']}}" required placeholder="Enter Content Mandarin Name" class="form-control" id="name_md">
                                @error('name_md')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name_hi">Name Hindi<span class="text-danger">*</span></label>
                                <input type="text" name="name_hi" parsley-trigger="change" value="{{$name['hi']}}" required placeholder="Enter Content Hindi Name" class="form-control" id="name_hi">
                                @error('name_hi')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name_sp">Name Spanish<span class="text-danger">*</span></label>
                                <input type="text" name="name_sp" parsley-trigger="change" value="{{$name['sp']}}" required placeholder="Enter Content Spanish Name" class="form-control" id="name_sp">
                                @error('name_sp')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name_fr">Name French<span class="text-danger">*</span></label>
                                <input type="text" name="name_fr" parsley-trigger="change" value="{{$name['fr']}}" required placeholder="Enter Content French Name" class="form-control" id="name_fr">
                                @error('name_fr')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Age Rating<span class="text-danger">*</span></label>
                                <select class="form-control select2" name="age_rating_id" required data-parsley-errors-container="#age_rating_id_error">
                                    <option disabled selected>Select Age Rating </option>
                                    @foreach ($age_rating as $value)
                                        <option value="{{ $value['id'] }}" <?php if($content->age_rating_id == $value['id']){ echo "selected"; } ?> > {{ $value['name'] }} </option>
                                    @endforeach
                                </select>
                                <div id="age_rating_id_error"></div>
                                @error('age_rating_id')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Year<span class="text-danger">*</span></label>
                                <input type="text" name="year" value="{{ $content->year }}" required  class="form-control" id="content_year" />
                                @error('year')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="synopsis_en">Synopsis English<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="synopsis_en" id="content_synopsis" required placeholder="Enter Content Synopsis English" >{{$synopsis['en']}}</textarea>

                                @error('synopsis_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="synopsis_md">Synopsis Mandarin<span class="text-danger">*</span></label>
                                
                                <textarea class="form-control" name="synopsis_md" id="content_synopsis" required placeholder="Enter Content Synopsis Mandarin" >{{$synopsis['md']}}</textarea>

                                @error('synopsis_md')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="synopsis_hi">Synopsis Hindi<span class="text-danger">*</span></label>
                                
                                <textarea class="form-control" name="synopsis_hi" id="content_synopsis" required placeholder="Enter Content Synopsis Hindi" >{{$synopsis['hi']}}</textarea>

                                @error('synopsis_hi')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="synopsis_sp">Synopsis Spanish<span class="text-danger">*</span></label>
                                
                                <textarea class="form-control" name="synopsis_sp" id="content_synopsis" required placeholder="Enter Content Synopsis Spanish" >{{$synopsis['sp']}}</textarea>

                                @error('synopsis_sp')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="synopsis_fr">Synopsis French<span class="text-danger">*</span></label>
                                
                                <textarea class="form-control" name="synopsis_fr" id="content_synopsis" required placeholder="Enter Content Synopsis French" >{{$synopsis['fr']}}</textarea>

                                @error('synopsis_fr')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Artist<span class="text-danger">*</span></label>
                                <select class="form-control select2 artist_ids" name="artist_ids[]" required data-parsley-errors-container="#artist_ids_error" multiple>
                                    <option disabled >Select Artist</option>

                                    @foreach ($artist as $key => $value)
                                        <option value="{{ $value->id }}" <?php if(in_array($value->id,$artist_ids)){ echo "selected"; } ?> >{{ $value['name'] }}</option>
                                    @endforeach
                                </select>
                                <div id="artist_ids_error"></div>
                                @error('artist_ids')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Category<span class="text-danger">*</span></label>
                                <select class="form-control select2 genre_ids" name="genre_ids[]" required data-parsley-errors-container="#genre_ids_error" multiple>
                                    <option disabled >Select category</option>
                                    @foreach ($genre as $key => $value)
                                        <option value="{{ $value->id }}" <?php if(in_array($value->id,$content_genres_ids)){ echo "selected"; } ?>  >{{ $value['name'] }}</option>
                                    @endforeach
                                </select>
                                <div id="genre_ids_error"></div>
                                @error('genre_ids')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Tags<span class="text-danger">*</span></label>
                                <select class="form-control select2 tag_ids" name="tag_ids[]" required data-parsley-errors-container="#tag_ids_error" multiple>
                                    <option disabled >Select tag</option>
                                    @foreach ($tag as $key => $value)
                                        <option value="{{ $value->id }}" <?php if(in_array($value->id,$content_tags_ids)){ echo "selected"; } ?> >{{ $value['name'] }}</option>
                                    @endforeach
                                </select>
                                <div id="tag_ids_error"></div>
                                @error('genre_ids')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Content Type<span class="text-danger">*</span></label>
                                <select class="form-control select2" name="content_type" required data-parsley-errors-container="#content_type_error">
                                    <option disabled selected>Select Content Type</option>
                                    @foreach ($content_type as $key => $value)
                                        <option value="{{ $key }}" <?php if($key == $content->content_type){ echo "selected"; } ?>  >{{ $value }}</option>
                                    @endforeach
                                </select>
                                <div id="content_type_error"></div>
                                @error('content_type')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Status<span class="text-danger">*</span></label>
                                <select class="form-control select2" name="status" required data-parsley-errors-container="#status_error" > 
                                    <option disabled selected>Select Status</option>
                                     @foreach ($content_status as $key => $value)
                                        <option value="{{ $key }}" <?php if($key == $content->status){ echo "selected"; } ?> >{{ $value }}</option>
                                    @endforeach                                    
                                </select>
                                <div id="status_error"></div>
                                @error('status')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Is free?</label>
                                <label class="switch">
                                  <input type="checkbox" <?php if($content->is_free == 1){ echo "checked"; } ?> name="is_free" class="form-control">
                                  <span class="slider round"></span>
                                </label>
                                @error('status')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        @php
                            $default = '/images/default.png';
                        @endphp
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Poster</label>
                                
                                <input type="file" name="poster"  class="form-control" id="content_poster" data-parsley-trigger="change"  data-parsley-max-file-size="5" data-parsley-filemimetypes="image/jpeg, image/png" accept="image/*" data-parsley-file-mime-types-message="Only allowed jpeg & png files" onchange="readURL1(this);">

                                <input type="hidden" name="old_poster" class="form-control" value="{{ !empty($content->poster)?$content->poster:'' }}">

                                @error('poster')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="col-lg-6">
                            <div class="form-group">
                                    <img class="border rounded p-0"  src="{{!empty($content->poster)?$content->poster:''}}" onerror="this.src='{{$default}}'" alt="your image" style="height: 130px;width: 130px; object-fit: cover;" id="blah1"/>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Logo</label>
                                
                                <input type="file" name="logo"  class="form-control" id="content_logo" data-parsley-trigger="change"  data-parsley-max-file-size="5" data-parsley-filemimetypes="image/jpeg, image/png" accept="image/*" data-parsley-file-mime-types-message="Only allowed jpeg & png files" onchange="readURL2(this);">

                                <input type="hidden" name="old_logo" class="form-control" value="{{ !empty($content->logo)?$content->logo:'' }}">

                                @error('logo')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <img class="border rounded p-0"  src="{{!empty($content->logo)?$content->logo:''}}" onerror="this.src='{{$default}}'" alt="your image" style="height: 130px;width: 130px; object-fit: cover;" id="blah2"/>
                                </div>
                            </div>

                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Trailer</label>
                                <input type="file" id="browseFile" class="form-control"  data-method = "trailer_file_upload" placeholder="select file" accept="video/*">
                                <input type="hidden" name="trailer_link" id="video_link" value="">
                                <input type="button" class="btn btn-primary mt-2" id="upToggle" disabled value="Pause OR Continue"/>
                            </div>
                            
                            <div  style="display: none" class="progress mt-3" style="height: 25px">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%; height: 100%">75%</div>
                            </div>

                            <?php if(!empty($content->trailer)){ ?>
                                <div class="card-footer p-4 mt-2" >
                                    <video id="videoPreview" src="{{ $content->trailer }}" controls style="width: 100%; height: auto;"></video>
                                </div>
                            <?php } ?>

                        </div>

                    </div>

                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            Submit
                        </button>
                        <a href="{{ route('admin.content.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a>
                    </div>
                </form>
				</div>
			</div>	
		</div>
	</div>
</div>

@endsection