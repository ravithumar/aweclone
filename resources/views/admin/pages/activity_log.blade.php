<!DOCTYPE html>
<html lang="en">
<head>
  <title>AWE</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
  <h1>Viewing Activity</h1>
</div>
  
<div class="container">
  <div class="row">
    <table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">Date & Time</th>
        <th scope="col">Content</th>
        <th scope="col">Episode</th>
      </tr>
    </thead>
    <tbody>
      @if(count($activity_log)>0)
        @foreach($activity_log as $key => $value)
          <tr>
            <th scope="row">{{ date('m-d-Y H:i A',strtotime($value->created_at)) }}</th>
            <td>{{ $value->content->name }}</td>
            <td>{{ $value->content->video->episode_name }}</td>
          </tr>
        @endforeach
      @else
        <tr>
          <th colspan="3" class="text-center">
            No any data found
          </th>
        </tr>
      @endif
     
    </tbody>
  </table>
  </div>
</div>

</body>
</html>
