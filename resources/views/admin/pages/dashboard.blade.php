@extends('admin.layouts.master')
@section('css')
<link href="{{url('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

                    <!-- Start Content-->
    <div class="container-fluid">
        
        <!-- start page title -->
        @include('admin.include.flash-message')
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
        </div>     
        <!-- end page title --> 
        
        <div class="row">
            
            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle  border-danger border">
                                <i class="fe-dollar-sign font-22 avatar-title text-danger"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">0</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Earnings</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->
            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle border-primary border">
                                <i class="fas fa-users font-22 avatar-title text-primary"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$users}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Customers</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle border-success border">
                                <i class="fas fa-school font-22 avatar-title text-success"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$content}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Content</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle border-success border">
                                <i class="fas fa-school font-22 avatar-title text-success"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$plan}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Plan</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle  border-warning border">
                                <i class="fe-shopping-cart font-22 avatar-title text-warning"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$total_sub}}</span>%</h3>
                                <p class="text-muted mb-1 text-truncate">Total Subscription </p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle  border-warning border">
                                <i class="fe-shopping-cart font-22 avatar-title text-warning"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$total_non_sub}}</span>%</h3>
                                <p class="text-muted mb-1 text-truncate">Total Non-Subscription </p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

        </div>


    <div class="row">
        
        <div class="col-md-6">
            <div class="card-box text-center">
                <div class="page-title-box text-left">
                    <h4 class="page-title">Highest Top 10 Contents</h4>
                </div>
                
                <table class="table">
                <thead>
                  <tr>
                    <th class="text-left">Content Name</th>
                    <th class="text-left">Total Views</th>
                  </tr>
                </thead>
                <tbody>
                    <?php if(!empty($most_viewed) && count($most_viewed)>0){ 
                        foreach ($most_viewed as $key => $value) { ?>
                        <tr>
                          <td class="text-left"><a href="{{route('admin.content.show',$value['content']['id'])}}">{{ $value['content']['name'] }}</a></td>
                          <td class="text-left">{{ $value['total_content'] }}</td>
                        </tr>
                    <?php } }else{ ?> 
                        <tr>
                          <td class="text-center"><strong>No data found.</strong></td>
                        </tr>
                    <?php } ?>
                    
                </tbody>
              </table>      
            </div>
        </div>
        
    </div>

    </div> <!-- container -->

@endsection

@section('script')
        <script src="{{url('assets/libs/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{url('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
        <script src="{{url('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{url('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
        <script src="{{url('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{url('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{url('assets/libs/datatables/buttons.html5.min.js')}}"></script>
        <script src="{{url('assets/libs/datatables/buttons.flash.min.js')}}"></script>
        <script src="{{url('assets/libs/datatables/buttons.print.min.js')}}"></script>
        <script src="{{url('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
        <script src="{{url('assets/libs/datatables/dataTables.select.min.js')}}"></script>
        <script src="{{url('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <script src="{{url('assets/libs/pdfmake/vfs_fonts.js')}}"></script>
        <script src="{{url('assets/js/pages/datatables.init.js')}}"></script>
@endsection
