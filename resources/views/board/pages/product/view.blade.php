@extends('board.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('viewproduct')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                            <tr>
                                <th class="text-nowrap" scope="row">Product Name</th>
                                <td colspan="5">{{$product->name}}</td>
                            </tr>
                            
                            <tr>
                                <th class="text-nowrap" scope="row">Category Name</th>
                                <td colspan="5">{{$product->category_name}}</td>
                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row">Discount</th>
                                <td colspan="5">{{number_format((float)$product->discount, 2, '.', '')}}%</td>

                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row">Business Discount</th>
                                <td colspan="5">{{number_format((float)$product->business_discount, 2, '.', '')}}%</td>
                            </tr>                            
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>	
		</div>
	</div>

    <div class="row">

    @foreach($storeproducts as $variant)
        <div class="col-xl-3">
            <div class="card">
              <img class="card-img-top" style="height: 219px;object-fit: contain;" src="{{$variant->variant->picture}}" alt="Card image cap">
              <!-- <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
              </div> -->
              <ul class="list-group list-group-flush">
                <li class="list-group-item">Stock : {{$variant->stock}} </li>
                <li class="list-group-item">Quantity : {{$variant->variant->quantity}}{{$variant->measurement}} </li>
                <li class="list-group-item">Price : ${{$variant->variant->price}} </li>
                <li class="list-group-item">Business Price : ${{$variant->variant->business_price}}</li>
                <li class="list-group-item">Age : {{$variant->variant->age}}</li>
                <li class="list-group-item">Proof : {{$variant->variant->proof}}%</li>
                <li class="list-group-item">Item Code : {{$variant->variant->item_code}}</li>

              </ul>
              <!-- <div class="card-body">
                <a href="#" class="card-link">Card link</a>
                <a href="#" class="card-link">Another link</a>
              </div> -->
            </div>
                
        </div>
            @endforeach
    </div>
</div>


@endsection
