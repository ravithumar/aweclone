<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StoresProduct extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'store_products';
    protected $fillable = [
        'user_id',
        'product_id',
        'stock'
    ];

    public function product() {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
        // return $this->hasMany('App\Models\Product', 'id', 'product_id');
    }

    public function variant() {
        return $this->hasOne('App\Models\Variant', 'id', 'variant_id');
    }

    public function store()
   {
      return $this->hasMany('App\Models\User', 'id', 'user_id');
   }  

}
