<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FavoriteVideo extends Model {
    
    use HasFactory, SoftDeletes;

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    protected $table = 'favorite_video';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function content() {
        return $this->hasmany('App\Models\Content', 'id', 'content_id');
    }
    
}
