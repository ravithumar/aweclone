<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
         'user_id',
         'lat',
         'log',
         'zipcode',
         'city',
         'state',
         'address_type',
         'complete_address',
    ];

     protected $casts = [        
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    protected $table = 'address';
}
