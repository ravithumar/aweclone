<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Artist extends Model
{

    use HasFactory, SoftDeletes, HasTranslations;
    protected $table = 'artists';
    protected $fillable = [
        'cast_type_id',
        'name'
    ];

    public $translatable = ['name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function castType()
    {
        return $this->belongsTo(CastTypes::class, 'cast_type_id');
    }
    
}
