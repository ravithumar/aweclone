<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContentGenres extends Model
{

    use HasFactory;
    public $timestamps = false;
    protected $table = 'content_genres';
    protected $fillable = [
        'content_id',
        'genre_id'
    ];


    public function genre()
    {
        return $this->belongsTo('App\Models\Genre', 'genre_id', 'id');
    }

    public function content()
    {
        return $this->belongsTo('App\Models\Content', 'content_id', 'id');
    }

}
