<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PreferredLanguage extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'preferred_language';
    protected $fillable = [
        'user_id',
        'language_id',
    ];

    public function language() {
        return $this->hasOne('App\Models\Language', 'id', 'language_id');
    }
}
