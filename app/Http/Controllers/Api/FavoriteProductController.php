<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\FavoriteProduct;
use App\Models\Product;
use App\Models\Variant;
use App\Models\StoresProduct;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FavoriteProductController extends Controller {
    use ApiResponser;

    public function index(Request $request) {

        $favorite_products = FavoriteProduct::whereUserId(request()->user()->id)->with(['store'])->orderBy('id', 'DESC')->get(); 
        $store_data = array();
        if($favorite_products){
            $stores = $favorite_products->pluck('store_id')->toArray();
            $products = $favorite_products->pluck('product_id')->toArray();

            $stores = array_unique($stores);
            $stores = array_values($stores);

            $temp = [];
            foreach ($favorite_products as $key => $value) {
                foreach ($stores as $s_key => $s_value) {
                    if($value->store_id == $s_value){
                        if(!in_array($s_value, $temp)){
                            $store_data[] = array(
                                'id' => $s_value,
                                'store_name' => $value->store->first_name.' '.$value->store->last_name,
                                'view_all' => FALSE,
                                'address' => $value->store->address,
                                'product_ids' => [],
                                'products' => []
                            );
                        }
                        
                        array_push($temp, $s_value);
                    }
                }
            }

            foreach ($store_data as $key => $value) {
                foreach ($favorite_products as $f_key => $f_value) {
                    if($value['id'] == $f_value->store_id){
                        array_push($store_data[$key]['product_ids'], $f_value->product_id);
                    }
                }
            }
            $product_data = Product::whereStatus("1")->orderBy('id','DESC')->whereIn('id', $products)->with(['varient'])->get();

            foreach ($store_data as $key => $value) {
                foreach ($product_data as $v_key => $v_value) {
                    $v_value->favorite = TRUE;
                    if(in_array($v_value->id, $value['product_ids'])){
                        if(count($store_data[$key]['products']) <= 10){
                            $store_data[$key]['products'][] = $v_value;
                        }else{
                            $store_data[$key]['view_all'] = TRUE;
                        }
                        
                    }
                }
                unset($store_data[$key]['product_ids']);
            }

        }
                
        return response([
            'status' => true,
            'message' => '',
            'data' => $store_data,
        ]);

    }

    /**
     *  makes a service provider favourite or removes him from favourite
     */

    public function add(Request $request) {
        $validator = Validator::make(request()->all(), [
            'product_id' => 'required',
            'store_id' => 'required',
        ]);

        if (!$validator->fails()) {
            if (Product::whereId($request->product_id)->whereStatus('1')->first()) {
                if (FavoriteProduct::whereProductId($request->product_id)->whereUserId($request->user()->id)->whereStoreId($request->store_id)->first()) {
                    return response([
                        'status' => false,
                        'message' => 'product already exits',
                    ], 200);
                } else {
                    $favorite_product = new FavoriteProduct;
                    $favorite_product->user_id = $request->user()->id;
                    $favorite_product->product_id = $request->product_id;
                    $favorite_product->store_id = $request->store_id;
                    $favorite_product->save();

                    if (isset($favorite_product)) {
                        return response([
                            'status' => true,
                            'message' => 'product add successfully',
                        ], 200);
                    }
                }
            } else {
                return response([
                    'status' => false,
                    'message' => 'product not found',
                ], 200);
            }
        }
        return $this->errorResponse($validator->messages(), true);
    }

    /**



     *  returns the favorites of the customer



     */

    // public function remove(FavoriteProductRequest $request)

    public function remove(Request $request) {

        $validator = Validator::make(request()->all(), [

            'product_id' => 'required',

            'store_id' => 'required',

        ]);

        if (!$validator->fails()) {

            if (FavoriteProduct::whereProductId($request->product_id)->whereStoreId($request->store_id)->whereUserId($request->user()->id)->first()) {
                FavoriteProduct::whereProductId($request->product_id)->whereStoreId($request->store_id)->whereUserId($request->user()->id)->delete();
                return response([
                    'status' => true,
                    'message' => 'product remove successfully',
                ], 200);

            } else {
                return response([
                    'status' => false,
                    'message' => 'record not found',
                ], 200);
            }

        }

        return $this->errorResponse($validator->messages(), true);

    }

    public function product_view_all(Request $request) {

        $validator = Validator::make(request()->all(), [            
            'store_id' => 'required',
        ]);
        if (!$validator->fails()) {
            $user_id = $request->user()->id;
            $favorite_products = FavoriteProduct::whereUserId(request()->user()->id)->whereStoreId($request['store_id'])->with(['store'])->orderBy('id', 'DESC')->get(); 
            $store_data = array();
            $store_data_ = array();
            if($favorite_products){
                $stores = $favorite_products->pluck('store_id')->toArray();
                $products = $favorite_products->pluck('product_id')->toArray();

                $stores = array_unique($stores);
                $stores = array_values($stores);

                $temp = [];
                foreach ($favorite_products as $key => $value) {
                    foreach ($stores as $s_key => $s_value) {
                        if($value->store_id == $s_value){
                            if(!in_array($s_value, $temp)){
                                $store_data_ = array(
                                    'id' => $s_value,
                                    'store_name' => $value->store->first_name.' '.$value->store->last_name,
                                    'product_ids' => [],
                                    'products' => []
                                );
                            }
                            
                            array_push($temp, $s_value);
                        }
                    }
                }

                // foreach ($store_data as $key => $value) {
                    foreach ($favorite_products as $f_key => $f_value) {
                        if($store_data_['id'] == $f_value->store_id){
                            array_push($store_data_['product_ids'], $f_value->product_id);
                        }
                    }
                // }
                $product_data = Product::whereStatus("1")->orderBy('id','DESC')->whereIn('id', $products)->with(['varient'])->get();

                // foreach ($store_data as $key => $value) {
                    foreach ($product_data as $v_key => $v_value) {
                        if(in_array($v_value->id, $store_data_['product_ids'])){
                            $store_data[] = $v_value;
                            // $store_data['products'][] = $v_value;
                        }
                    }
                    unset($store_data['product_ids']);
                // }
            }
                    
            return response([
                'status' => true,
                'message' => 'successfully',
                'data' => $store_data,
            ]);

        }

        return $this->errorResponse($validator->messages(), true);

    }

}
