<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Plan;
use App\Models\UsersGroup;
use App\Models\Devices;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\UserResource;
use CommonHelper;
use Carbon\Carbon;
use Mail;
use App\Mail\AccountDeleteMail;

class UserController extends Controller {

    use ApiResponser;


    public function register_device_list(Request $request) {
       
       $user = User::whereId($request->user()->id)->where('status',1)->first();
       if($user->parent_id == '0'){
            $user_id = $request->user()->id;
       }else{
            $user_id = $user->parent_id;
       }

        $device_list = Devices::where('user_id',$user_id)->orderBy('id', 'desc')->get();

        return response([
            'status' => true,
            'message' =>'',
            'data' =>$device_list
        ]);

    }


    public function deregister_user(Request $request) {
       
        $validator = Validator::make(request()->all(), [
            'devices_id' => 'required|integer'
        ]);

        if (!$validator->fails()) {
            $device = Devices::find($request->devices_id);
            //$user = Users::find($device->user_id);
            //$user->revokeAllTokens();

            $device->delete();

            return response([
                'status' => true,
                'message' => __('message.deregister_device_sucess'),
            ]);
        }

       return $this->errorResponse($validator->messages(), true);

    }

    public function add(Request $request) {
       
        $validator = Validator::make(request()->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'is_child' => 'required|integer|between:0,1'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{

            $user = User::find($request->user()->id);

            if($user->parent_id == 0){
                $user_count = User::where('parent_id', $user->id)->count();
                $parent_id = $user->id;
            }else{
                $user_count = User::where('parent_id', $user->parent_id)->count();
                $parent_id = $user->parent_id;
            }

            if($user_count >= 4){
                return response([
                    'status' => false,
                    'message' => 'Account limit is 5'
                ]);
            }

            $new_user = new User(); 
            $new_user->parent_id = $parent_id;
            $new_user->email = $user->email;
            $new_user->phone = $user->phone;
            $new_user->phone_verified = $user->phone_verified;
            //$new_user->email_verified = $user->email_verified;
            $new_user->password = '';
            $new_user->status = 1;
            $new_user->is_child = $request->is_child;
            $new_user->first_name = $request->first_name;
            $new_user->last_name = $request->last_name;
            $new_user->save();

            UsersGroup::create([
                'user_id' => $new_user->id,
                'group_id' => '4',
            ]);

            if(isset($request->profile_picture) && !empty($request->profile_picture)){
                if ($request->hasFile('profile_picture')) {
                    $dir = env('AWS_S3_MODE')."/users";
                    $profile_picture = CommonHelper::s3Upload($request->file('profile_picture'), $dir);
             
                    $update_data['profile_picture'] = $profile_picture;

                    User::where('id',$new_user->id)->update($update_data);
                }
            }

            return ([
                'status' => true,
                'data' => User::find($new_user->id),
                'message' => ''
            ]);
        }

    }

    public function remove(Request $request) {
       
        $validator = Validator::make(request()->all(), [
            'user_id' => 'required'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{

            $user = User::find($request->user_id);

            if($user->parent_id == 0){
                
                $title = "Account Deleted";
                $description = "<p>Your account will be permanently removed from the system after 30 days from today. Contact the administrator in case you want to re-activate your account.</p>";
        
                User::where('id', $user->id)->update(['status'=> 2, 'deactivated_time' => Carbon::now()]);

                User::where('parent_id', $user->id)->update(['status'=> 2 ]);

                $user->revokeAllTokens();
                $users = User::where('parent_id', $user->id)->get();
                if(isset($users) && count($users) > 0){
                    foreach ($users as $key => $value) {
                        $value->revokeAllTokens();
                    }
                }

                Mail::to($user->email)->send(new AccountDeleteMail($title,$description, config('app.address1'), config('app.address2')));

            }else{
                $user->revokeAllTokens();
                User::where('id', $user->id)->delete();
            }

            return response([
                'status' => TRUE,
                'message' => 'Account deleted successfully'
            ]);
        }

    }

    public function generate_passport_token(Request $request){

        $validator = Validator::make(request()->all(), [
            'user_id' => 'required'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{

            $user = User::find($request->user_id);

            if($user){
                $user->token = $user->createToken('authToken')->accessToken;

                return (new UserResource($user))->additional([
                        'status' => true,
                        'message' => ''
                    ]);
            }else{
                return response([
                    'status' => false,
                    'message' => __('message.user_not_found'),
                ]);
            }
            
        }

    }

    public function add_lock(Request $request){

        $validator = Validator::make(request()->all(), [
            'user_id' => 'required|numeric',
            'pin' => 'required|numeric|min:4'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{

            $user = User::find($request->user_id);
            $user->lock_password = $request->pin;
            $user->save();

            return ([
                'status' => true,
                'message' => __('message.lock_added')
            ]);
        
        }

    }

    public function verify_lock(Request $request){

        $validator = Validator::make(request()->all(), [
            'user_id' => 'required',
            'pin' => 'required|numeric|min:4'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{

            $user = User::whereId($request->user_id)->where('lock_password', $request->pin)->first();

            if($user){

                return ([
                    'status' => true,
                    'message' => __('message.verified')
                ]);
            }else{
                return response([
                    'status' => false,
                    'message' => __('message.incorrect_pin')
                ]);
            }
            
        }

    }


    public function disable_pin(Request $request){

        $validator = Validator::make(request()->all(), [
            'user_id' => 'required'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{

            $user = User::whereId($request->user_id)->where('status',1)->first();

            if($user){
                
                User::whereId($request->user_id)->update(['lock_password'=>null]);

                return ([
                    'status' => true,
                    'message' => __('message.disable_pin')
                ]);
            }else{
                return response([
                    'status' => false,
                    'message' => __('message.user_not_found'),
                ]);
            }
        }
    }

    public function change_pin(Request $request){

        $validator = Validator::make(request()->all(), [
            'user_id' => 'required',
            'pin' => 'required|min:4|max:4'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{

            $user = User::whereId($request->user_id)->where('status',1)->first();

            if($user){
                
                User::whereId($request->user_id)->update(['lock_password'=>$request->pin]);

                return ([
                    'status' => true,
                    'message' => __('message.change_pin')
                ]);
            }else{
                return response([
                    'status' => false,
                    'message' => __('message.user_not_found'),
                ]);
            }
            
        }

    }


    public function login_activity(Request $request){

        $validator = Validator::make(request()->all(), [
            'latitude' => 'required',
            'longitude' => 'required',
            'device_name' => 'required',
            'device_token' => 'required',
            'device_type' => 'required'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{
            
            $user_data = auth()->user();

            $user = User::whereId($user_data->id)->where('status',1)->first();

            $map_key = CommonHelper::ConfigGet('map_key');

            if($user){

                $geocode = @file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$request->latitude.','.$request->longitude.'&sensor=true&key='.$map_key.'');

                if($geocode === FALSE){
                    return ([
                            'status' => false,
                            'message' => __('message.something_went_wrong')
                        ]);
                }

                $data = json_decode($geocode);
                if(empty($data->results)){
                    return ([
                            'status' => false,
                            'message' => __('message.something_went_wrong')
                        ]);
                }else{

                    $add_array  = $data->results;
                    $add_array = $add_array[0];
                    $add_array = $add_array->address_components;
                    $country = "";
                    $state = ""; 
                    $city = "";

                        foreach ($add_array as $key) {
                          if($key->types[0] == 'administrative_area_level_2')
                          {
                            $city = $key->long_name;
                          }
                          if($key->types[0] == 'administrative_area_level_1')
                          {
                            $state = $key->long_name;
                          }
                          if($key->types[0] == 'country')
                          {
                            $country = $key->long_name;
                          }
                        }
                }

                

                $devices_data = Devices::where('user_id',$user_data->id)
                ->where('name',$request->device_name)
                ->where('token',$request->device_token)
                ->where('type',$request->device_type)
                ->first();

                if(empty($devices_data)){

                    $devices = Devices::insert([
                        'user_id' => $user_data->id,
                        'name' => $request->device_name,
                        'token' => $request->device_token,
                        'type' => $request->device_type,
                        'city' => $city,
                        'state' => $state,
                        'country' => $country,
                        'login_status' => 1,
                        'latitude' => $request->latitude,
                        'longitude' => $request->longitude
                    ]);

                    if($devices){
                        return ([
                            'status' => true,
                            'message' => 'Log generated'
                        ]);
                    }else{
                        return ([
                            'status' => false,
                            'message' => 'Log not generated'
                        ]);
                    }

                }else{
                    $devices = Devices::where('id',$devices_data->id)->update([
                        'latitude' => $request->latitude,
                        'longitude' => $request->longitude
                    ]); 

                    if($devices){
                        return ([
                            'status' => true,
                            'message' => 'Log update sucessfully'
                        ]);
                    }else{
                        return ([
                            'status' => false,
                            'message' => 'Log not generated'
                        ]);
                    }

                }


                

            }else{
                return response([
                    'status' => false,
                    'message' => __('message.user_not_found'),
                ]);
            }
            
        }

    }

    public function subscription_plan(Request $request){

        $validator = Validator::make(request()->all(), [
            'plan_id' => 'required|numeric'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{

            $user_data = auth()->user();

            if($user_data->parent_id != 0){
                $user_data = User::whereId($user_data->parent_id)->where('status',1)->first();
            }

                if(!empty($user_data->plan_id)){
                    
                    $update_data['plan_id'] = $request->plan_id;
                    $update_data['expiration_date'] = Carbon::createFromFormat('Y-m-d', $user_data->expiration_date)->addDays(30);

                }else{

                    $plan = Plan::where('status',1)->first();

                    $update_data['plan_id'] = $request->plan_id;
                    $update_data['expiration_date'] = Carbon::now()->addDays(30);
                    
                }

                $update = User::where('id',$user_data->id)->update($update_data);  
                
                if($update){
                    return response([
                        'status' => true,
                        'message' => __('message.subscription_plan_added_sucess'),
                    ]);
                }else{
                    return response([
                        'status' => true,
                        'message' => __('message.something_went_wrong'),
                    ]);
                }
        }

    }

    public function save_language(Request $request){

        $validator = Validator::make(request()->all(), [
            'language_id' => 'required'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }else{

            $user_id = auth()->user()->id;

            $update_data['language_id'] = $request->language_id;

            User::whereId($user_id)->update($update_data);

            return response([
                'status' => true,
                'message' => 'Saved',
            ]);
        }

    }


}
