<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Feedback;
use App\Models\Faq;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FeedbackController extends Controller {

    public function add(Request $request) {
        $validator = Validator::make(request()->all(), [
            'faq_id' => 'required|integer',
            'description' => 'required'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }

        if (Faq::where('id',$request->faq_id)->where('status',1)->first()) {

            $feedback = new Feedback;
            $feedback->user_id = $request->user()->id;
            $feedback->faq_id = $request->faq_id;
            $feedback->description = $request->description;
            $feedback->save();

            return response([
                'status' => true,
                'message' => __('message.feedback_success')
            ]);
        }else{
            return response([
                'status' => true,
                'message' => __('message.faq_does_not_exist')
            ]);
        }

    }

}
