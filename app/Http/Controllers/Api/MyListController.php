<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Models\MyList;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MyListController extends Controller {

    use ApiResponser;


    public function index(Request $request) {


        //return $request->user()->tokens();
        $content = MyList::with(['content'=>function($q) {            
            $q->select('content.id','content.poster','content.is_free');
        }])->where('user_id',$request->user()->id)->orderBy('id','desc')->paginate(16);


        if($content){
            return response([
                'status' => true,
                'message' => '',
                'data' => $content
            ]);
        }else{
            return response([
                'status' => false,
                'message' => __('message.no_data_found')
            ]);
        }


    }


    public function status_update(Request $request) {

        $validator = Validator::make(request()->all(), [
            'content_id' => 'required',
            'status' => 'required|integer|between:0,1',
        ]);

        if (!$validator->fails()) {
            if (Content::whereId($request->content_id)->where('status','!=','2')->first()) {
                if($request->status == '1'){

                    if (MyList::whereContentId($request->content_id)->whereUserId($request->user()->id)->first()) {

                        return response([
                            'status' => false,
                            'message' => __('message.favorite_video_exits'), //'Video already exits',
                        ]);


                    } else {
                        $favorite_video = new MyList;
                        $favorite_video->user_id = $request->user()->id;
                        $favorite_video->content_id = $request->content_id;
                        $favorite_video->save();

                        return response([
                            'status' => true,
                            'message' => __('message.mylist_video_success'), // 'Video add successfully',
                        ]);

                    }
                }else{

                    MyList::whereContentId($request->content_id)->whereUserId($request->user()->id)->delete();
                    
                    return response([
                        'status' => true,
                        'message' => __('message.mylist_video_remove'), //'Video remove successfully',
                    ]);

                }
            } else {
                return response([
                    'status' => false,
                    'message' => __('message.video_not_found'), //'Video not found',
                ]);
            }
        }
        return $this->errorResponse($validator->messages(), true);

    }




}
