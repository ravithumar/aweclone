<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PlayBackController extends Controller {

    use ApiResponser;

    public function status_update(Request $request) {

        $validator = Validator::make(request()->all(), [
            'user_id' => 'required|integer',
            'autoplay' => 'required|integer|between:0,1',
            'autoplay_previews' => 'required|integer|between:0,1',
        ]);

        if (!$validator->fails()) {
            if (User::where('id',$request->user_id)->where('status','=','1')->first()) {
                
                    $user = User::where('id',$request->user_id)->update([
                                'autoplay'=>$request->autoplay,
                                'autoplay_previews'=>$request->autoplay_previews
                            ]);

                    if($user){
                        return response([
                            'status' => true,
                            'message' => __('message.playback_setting_update_success'), // 'Video add successfully',
                        ]);
                    }else{
                        return response([
                            'status' => false,
                            'message' => __('message.playback_setting_not_update'), // 'Video add successfully',
                        ]);
                    }
                        

            } else {
                return response([
                    'status' => false,
                    'message' => __('message.user_not_found'), //'Video not found',
                ]);
            }
        }
        return $this->errorResponse($validator->messages(), true);

    }

}
