<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;

use Illuminate\Http\Request;
use App\Models\Plan;
use App\Models\PlanAttribute;
use App\Models\Attribute;
use App\Models\Product;
use App\Models\User;
use DataTables;
use Illuminate\Support\Facades\Auth;

class PlanController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{

	   if ($request->ajax())
		{
			$data = Plan::get();
			return Datatables::of($data)
			->addIndexColumn()
			->editColumn('action', function ($row){
				$btn = '<a href="'.route('admin.plan.edit',$row['id']).'" class="mr-2"><i class="fa fa-edit"></i></a>';
				$btn .= '<a href="'.route('admin.plan.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
				$btn .= '<a href="'.route('admin.plan.destroy', $row['id']).'" class="mr-2" data-url="plan" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
				$btn .= '<a href="'.route('admin.plan.manageattr',$row['id']).'" class="mr-2"><i class="fas fa-tasks"></i></a>';
				return $btn;
			})
			->editColumn('name', function ($row){
				return $row['name'];
			})
			->editColumn('price', function ($row){
				return '$ '.$row['price'];
			})
			// ->editColumn('status', function ($row)
			// {
			// 	if($row['status'] == 0){
			// 		return '<button onclick="active_deactive_plan(this);" data-id="' . $row['id'] . '" data-token="' . csrf_token() . '"  class="btn btn-danger btn-xs waves-effect waves-light" data-table="plan" data-status="' . $row['status']. '">Inactive</button>';
			// 	}else{
			// 		return '<button onclick="active_deactive_plan(this);" data-id="' . $row['id'] . '" data-token="' . csrf_token() . '"   class="btn btn-success btn-xs waves-effect waves-light" data-table="plan" data-status="' . $row['status']. '">Active</button>';
			// 	}
			// })
			->rawColumns(['image', 'action','status'])
			->make(true);
	   }
	   else
	   {
		   $columns = [
			   ['data' => 'id','name' => 'id','title' => "Id"], 
			   ['data' => 'name','name' => 'name', 'title' => __("Name")],
			   ['data' => 'price','name' => 'price', 'title' => __("Price"), 'searchable'=>true],
			   // ['data' => 'status','title' => __("Status"),'searchable'=>false],
			   ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
		   $params['dateTableFields'] = $columns;
		   $params['dateTableUrl'] = route('admin.plan.index');
		   $params['dateTableTitle'] = "Plan Management";
		   $params['dataTableId'] = time();
		   $params['addUrl'] = route('admin.plan.create');
		   return view('admin.pages.plan.index',$params);
	   }
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$params['pageTittle'] = "Add Plan" ;
		$params['backUrl'] = route('admin.plan.index');
		return view('admin.pages.plan.post',$params);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		
		
		$request->validate([
			'name_en' => 'required',
			'name_hi' => 'required',
			'name_fr' => 'required',
			'name_md' => 'required',
			'name_sp' => 'required',
			'price' => 'required'
		]);
		 

		$plan = Plan::create([
			'name' => [
				'en' => $request->name_en,
      			'hi' => $request->name_hi,
      			'fr' => $request->name_fr,
      			'md' => $request->name_md,
      			'sp' => $request->name_sp
			],
			'price' => $request->price
		]);

		$attributes = Attribute::all();
		$insert_data = [];
		foreach ($attributes as $key => $value) {
			$data = array(
				'plan_id' => $plan->id,
				'attribute_id' => $value->id,
				'value' => 0,
			);
			array_push($insert_data, $data);
		}
		PlanAttribute::insert($insert_data);
		
		// redirect
		return redirect()->route('admin.plan.index')->with('success','Plan created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$params['pageTittle'] = "View Plan" ;
		$params['plan'] = Plan::find($id);
		$params['backUrl'] = route('admin.plan.index');
		return view('admin.pages.plan.view',$params);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$params['pageTittle'] = "Edit Plan";
		$params['plan'] = Plan::find($id);
		$params['backUrl'] = route('admin.plan.index');
		return view('admin.pages.plan.put',$params);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
	
		$request->validate([
			'name_en' => 'required',
			'name_hi' => 'required',
			'name_fr' => 'required',
			'name_md' => 'required',
			'name_sp' => 'required',
			'price' => 'required'
			
		]);

		$plan['name'] = array(
				'en' => $request->name_en,
      			'hi' => $request->name_hi,
      			'fr' => $request->name_fr,
      			'md' => $request->name_md,
      			'sp' => $request->name_sp
			);
		
		$plan['price'] = $request->price;
	
		Plan::whereId($id)->update($plan);
	
		return redirect()->route('admin.plan.index')
						->with('success','Plan updated successfully');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		// Plan::whereId($id)->delete();
		$products = Product::wherePlanId($id)->get();		
		if (isset($products) && !empty($products) && count($products) > 0) {
            $data['status'] = false;
			$data['message'] = 'You can not delete as Product(s) already exist for this plan';        
		} else
		{
			Plan::whereId($id)->delete();            
			$data['status'] = TRUE;        
			$data['message'] = 'Deleted';
		}
        echo json_encode($data);
		// return redirect()->route('admin.plan.index')->with('success','Plan deleted successfully');

	}

	public function active_deactive_plan()
	{
		if($_POST['table'] == 'plan'){
			if($_POST['status'] == 0){
				$status = 1;
			}else{
				$status = 0;
			}
			Plan::where('id', $_POST['id'])->update(['status' => $status]);
		}
		echo $status;
	}

	public function manageattr($id){
		$plan = Plan::find($id);
		$params['pageTittle'] = "Manage ".$plan->name." Plan Attributes";
		$params['plan_attributes'] = PlanAttribute::where('plan_id', $id)->with(['attribute'])->get();
		$params['plan_id'] = $id;
		//dd($params['plan_attributes']);
		$params['backUrl'] = route('admin.plan.index');
		return view('admin.pages.plan.manageattr',$params);
	}

	public function attr_update(Request $request, $id)
	{
		
		$attributes = Attribute::all();
		foreach ($attributes as $key => $value) {
			$update_data = array(
				'value' => $request[$value->id]
			);
			PlanAttribute::where('plan_id', $id)->where('attribute_id', $value->id)->update($update_data);
		}
	
		return redirect()->route('admin.plan.index')
						->with('success','Plan attributes updated successfully');

	}
}
