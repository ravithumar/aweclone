<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Models\Faq;
use DataTables;
use Illuminate\Support\Facades\Auth;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
            
       if ($request->ajax())
       {
            $data = Faq::select('*')->get();

            return Datatables::of($data)
            ->editColumn('status', function ($row)
            {
                if($row['status'] == 0){
                    return '<button onclick="active_deactive_faq(this);" data-id="' . $row['id'] . '" data-token="' . csrf_token() . '"  class="btn btn-danger btn-xs waves-effect waves-light" data-table="faq" data-status="' . $row['status']. '">Inactive</button>';
                }else{
                    return '<button onclick="active_deactive_faq(this);" data-id="' . $row['id'] . '" data-token="' . csrf_token() . '"   class="btn btn-success btn-xs waves-effect waves-light" data-table="faq" data-status="' . $row['status']. '">Active</button>';
                }
            })
            ->editColumn('question', function ($row){
                return strlen($row['question']) > 50 ? substr($row['question'],0,50)."..." : $row['question'];
            })
            ->editColumn('action', function ($row){
                $btn = '<a href="'.route('admin.faq.edit',$row['id']).'" class="mr-2"><i class="fa fa-edit"></i></a>';
                $btn .= '<a href="'.route('admin.faq.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
                $btn .= '<a href="'.route('admin.faq.destroy', $row['id']).'" data-url="faq" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
                
                return $btn;
            })
            ->rawColumns(['action','id','question','status'])
            
            ->make(true);
       }
       else
       {
           $columns = [
               ['data' => 'id','name' => 'id','title' => "Id"],
               ['data' => 'question', 'name' => 'question','title' => __("Question"),'searchable'=>true ], 
               ['data' => 'status', 'name' => 'status','title' => __("Status"),'searchable'=>true ], 
               ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
           $params['dateTableFields'] = $columns;
           $params['dateTableUrl'] = route('admin.faq.index');
           $params['dateTableTitle'] = "FAQ Management";
           $params['dataTableId'] = time();
           $params['addUrl'] = route('admin.faq.create');
           return view('admin.pages.faq.index',$params);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $params['pageTittle'] = "Add FAQ" ;
        $params['backUrl'] = route('admin.faq.index');

        return view('admin.pages.faq.post',$params);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'question_en' => 'required',
            'question_hi' => 'required',
            'question_fr' => 'required',
            'question_md' => 'required',
            'question_sp' => 'required',
            'answer_en' => 'required',
            'answer_hi' => 'required',
            'answer_fr' => 'required',
            'answer_md' => 'required',
            'answer_sp' => 'required',
        ]);
     
        $Faq = Faq::create([
            'question' => [
                'en' => $request->question_en,
                'hi' => $request->question_hi,
                'fr' => $request->question_fr,
                'md' => $request->question_md,
                'sp' => $request->question_sp
            ],
            'answer' => [
                'en' => $request->answer_en,
                'hi' => $request->answer_hi,
                'fr' => $request->answer_fr,
                'md' => $request->answer_md,
                'sp' => $request->answer_sp
            ],
        ]);

        return redirect()->route('admin.faq.index')->with('success','FAQ created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $params['pageTittle'] = "View FAQ";
        $params['faq'] = Faq::find($id);
        $params['backUrl'] = route('admin.faq.index');

        return view('admin.pages.faq.view',$params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
            
        $params['pageTittle'] = "Edit FAQ";
        $params['faq'] = Faq::find($id);
        $params['backUrl'] = route('admin.faq.index');
        return view('admin.pages.faq.put',$params);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        //dd($request->all());

        $request->validate([
            'question_en' => 'required',
            'question_hi' => 'required',
            'question_fr' => 'required',
            'question_md' => 'required',
            'question_sp' => 'required',
            'answer_en' => 'required',
            'answer_hi' => 'required',
            'answer_fr' => 'required',
            'answer_md' => 'required',
            'answer_sp' => 'required',
        ]);

        $faq = Faq::where('id', $id)->update([
            'question' => [
                'en' => $request->question_en,
                'hi' => $request->question_hi,
                'fr' => $request->question_fr,
                'md' => $request->question_md,
                'sp' => $request->question_sp
            ],
            'answer' => [
                'en' => $request->answer_en,
                'hi' => $request->answer_hi,
                'fr' => $request->answer_fr,
                'md' => $request->answer_md,
                'sp' => $request->answer_sp
            ],
        ]);

        return redirect()->route('admin.faq.index')->with('success','FAQ updated successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        Faq::whereId($id)->delete();
        return redirect()->route('admin.faq.index')
                        ->with('success','FAQ deleted successfully');
    }

    public function active_deactive_faq()
    {
       if($_POST['table'] == 'faq'){
            if($_POST['status'] == 0){
                $status = 1;
            }else{
                $status = 0;
            }
            Faq::where('id', $_POST['id'])->update(['status' => $status]);
        }
        echo $status; 
    }

}
