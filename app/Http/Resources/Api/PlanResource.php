<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class PlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {        
        $user_data = auth()->user();

        if($user_data->parent_id == 0){
            $plan_id = $user_data->plan_id;
        }else{
            $user = User::whereId($user_data->parent_id)->where('status',1)->first();
            $plan_id = $user->plan_id;
        }

        if($plan_id == $this->id){
            $selected = true;
        }else{
            $selected = false;
        }

       return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'selected' => $selected,
            'attribute' => PlanAttributeResource::collection($this->attributes)
        ];
       // return parent::toArray($request);
    }
}
