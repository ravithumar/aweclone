<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class PopularSearchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {   

        return [
            'id' => $this->id,
            'name' => $this->name,
            'is_free' => $this->is_free,
            'poster' => $this->poster,
            'trailer' => $this->trailer,
            'genres' => PopularSearchGenresResource::collection($this->genres)
        ];

    }
}
