<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class ContentCastResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'content_id' => $this->content_id,
            'artist_id' => $this->artist_id,
            'artist' => [
                'id'=>$this->artist->id,
                'cast_type_id'=>$this->artist->cast_type_id,
                'name'=>$this->artist->name
            ]

        ];
    }
}
